defmodule AnoPool do
  @moduledoc """
  Documentation for `AnoPool`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AnoPool.hello()
      :world

  """
  def hello do
    :world
  end
end
