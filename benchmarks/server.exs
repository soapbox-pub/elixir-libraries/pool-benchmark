defmodule AnoPool.Benchmark.CowboyHandler do
  def init(%{method: "GET"} = req, state) do
    bytes = :cowboy_req.binding(:bytes, req)
    bits = bytes * 8
    chunk_size = :cowboy_req.binding(:chunk_size, req, nil)

    unless chunk_size do
      req =
        :cowboy_req.reply(
          200,
          %{"content-type" => "application/octet-stream"},
          <<0::size(bits)>>,
          req
        )

      {:ok, req, state}
    else
      req = :cowboy_req.stream_reply(200, %{"content-type" => "application/octet-stream"}, req)
      req = stream_body(req, bytes, chunk_size)
      {:ok, req, state}
    end
  end

  defp stream_body(req, bytes, chunk_size) do
    if bytes - chunk_size < 0 do
      bits = bytes * 8
      :cowboy_req.stream_body(<<0::size(bits)>>, :fin, req)
    else
      bits = chunk_size * 8
      :cowboy_req.stream_body(<<0::size(bits)>>, :nofin, req)
      stream_body(req, bytes - chunk_size, chunk_size)
    end
  end
end

dispatch =
  :cowboy_router.compile([
    {:_,
     [
       {"/:bytes/[:chunk_size]", [{:bytes, :int}, {:chunk_size, :int}],
        AnoPool.Benchmark.CowboyHandler, %{}}
     ]}
  ])

{:ok, _} =
  :cowboy.start_clear(:benchmark_http_clear_listener, [port: 4545], %{
    env: %{dispatch: dispatch},
    protocols: [:http]
  })

{:ok, _} =
  :cowboy.start_clear(:benchmark_http2_clear_listener, [port: 4546], %{
    env: %{dispatch: dispatch},
    protocols: [:http2]
  })

ssl_dir = Path.join([File.cwd!(), "benchmarks", "ssl"])

ssl_opts = [
  certfile: Path.join([ssl_dir, "server.pem"])
]

{:ok, _} =
  :cowboy.start_tls(
    :benchmark_http_tls_listener,
    ssl_opts ++ [port: 4547],
    %{
      env: %{dispatch: dispatch},
      protocols: [:http]
    }
  )

{:ok, _} =
  :cowboy.start_tls(
    :benchmark_http2_tls_listener,
    ssl_opts ++ [port: 4548],
    %{
      env: %{dispatch: dispatch},
      protocols: [:http2]
    }
  )

# Assume all servers got started on the same interface
{addr, _} = :ranch.get_addr(:benchmark_http2_tls_listener)

IO.puts("Benchmark server started at #{inspect(addr)}")

Supervisor.start_link([], strategy: :one_for_one)
