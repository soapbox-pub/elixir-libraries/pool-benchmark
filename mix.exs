defmodule AnoPool.MixProject do
  use Mix.Project

  def project do
    [
      app: :ano_pool,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      preferred_cli_env: [bench: :bench],
      aliases: [bench: "run benchmarks/main.exs"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:cowboy, "~> 2.8", only: [:bench]},
      {:benchee, "~> 1.0", only: [:bench]},
      {:finch, "~> 0.3", only: [:bench]},
      {:hackney, "~> 1.16", only: [:bench]},
      {:gun,
       git: "https://github.com/ninenines/gun", ref: "921c47146b2d9567eac7e9a4d2ccc60fffd4f327"},
      {:cowlib, "~> 2.9.1", override: true}
    ]
  end
end
